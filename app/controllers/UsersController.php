<?php

class UsersController extends \BaseController {

	protected $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}

	public function index()
	{
		$users = $this->user->all();
		return View::make('users.index')->withUsers($users);
		// return View::make('users/index')->with('users', $users); // All mean the same thing
		// return View::make('users/index', ['users' => $users]);
	}

	public function show($username)
	{
		$user = $this->user->whereUsername($username)->first(); //Select * from users where username = USERNAME LIMIT 1
		return View::make('users.show')->withUser($user);
	}

	public function create()
	{
		return View::make('users.create');
	}

	public function store()
	{
		$input = array(
			'username' => Input::get('username'),
			'email'    => Input::get('email'),
			'password' => Hash::make(Input::get('password'))
		);

		if ( ! $this->user->fill($input)->isValid())
		{
			return Redirect::back()->withInput()->withErrors($this->user->errors);
		}

		$this->user->save();


		return Redirect::route('users.index');
	}
}

