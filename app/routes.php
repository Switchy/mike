<?php

//Route::get('users', 'UsersController@index');
//Route::get('users/{username}', 'UsersController@show');
Route::resource('users', 'UsersController');
Route::get('/', function()
{
	/*User::create([
		'username' => 'Switchy',
		'email' => 'switchytm@gmail.com',
		'password' => Hash::make('1234')
	]);*/
	return 'Done';
});




/* Previous lessons below, showing what kind of things we can do with Laravel
*  This should help me by allowing me to easily backtrack and see what I've done


Route::get('/', 'PagesController@home');

Route::get('/about', 'PagesController@about');

Route::get('users', function ()
{
	$users = User::all(); //select * from users
	$user = User::find(4); // Find a user with an ID of 4

	return $user->username; // Return a user's username, you may use email or any field you want
	return $user->email; // Returning a users email

	$user = new User;
	$user-> = 'NewUser';
	$user-> = Hash::make'password';

});


Route::get('/', function()
{
	$users = DB::table('users')->where('username', '!=', 'Switchy')->get();

	//dd($user); // die(var_dump($user));
	return $users;
});


  --> TWO WAYS TO CREATE A USER

	/*
	$user = new User;
	$user->username = 'UserOne';
	$user->password = Hash::make('password');
	$user->save();
	*/

	/*User::create([
		'username' => 'AnotherUser',
		'password' => Hash::make('1234')
		]);

  ---> DELETING A USER

	/*$user = User::find(7);
	$user->delete();

 ---> LIST IN ASCENDING ORDER 							||
   And also showing how many users you wish to see. \/

	return User::orderBy('username', 'asc')->take(2)->get();
	return User::orderBy('username', 'asc')->get();
*/